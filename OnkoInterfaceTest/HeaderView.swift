//
//  MainViewControllerExtension.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 03.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class HeaderView: UICollectionReusableView {
    
    // MARK:- Properties
    
    let topView = UIView()
    let topViewLabel = UILabel()
    let button1 = UIButton()
    let button2 = UIButton()
    
    // MARK:- Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        
        add(topView
            .backgroundColor(.topViewColor)
            .cornerRadius(10)) {
                $0.edges.equalToSuperview().inset(12)
        }
        
        topView.add(topViewLabel
            .numberOfLines(0)
            .boldFont(frame.width / 22)
            .fontColor(.white)
            .textAlignment(.center)
            .text("Пройдите опрос чтобы получать персонализированную подборку статей и мероприятий")) {
                $0.top.equalToSuperview().inset(24)
                $0.leading.trailing.equalToSuperview().inset(14)
                $0.height.equalToSuperview().multipliedBy(0.45)
        }
        
        topView.add(HStack([
            button1
                .backgroundColor(.button1Color)
                .cornerRadius(7)
                .title("Позже"),
            button2
                .backgroundColor(.button2Color)
                .cornerRadius(7)
                .title("Начать")
        ])
            .height(44)
            .alignment(.center)
            .distribution(.fillEqually)
            .spacing(12)) {
                $0.bottom.equalToSuperview().inset(16)
                $0.trailing.leading.equalToSuperview().inset(14)
        }
        
        button1.snp.makeConstraints {
            $0.height.equalToSuperview()
        }
        button2.snp.makeConstraints {
            $0.height.equalToSuperview()
        }
    }
}

//MARK:- Color Extension

extension UIColor {
    
    static var topViewColor: UIColor {
        #colorLiteral(red: 0.2078431373, green: 0.05882352941, blue: 0.5019607843, alpha: 1)
    }
    static var button1Color: UIColor {
        #colorLiteral(red: 0.3646321893, green: 0.2480854094, blue: 0.600522697, alpha: 1)
    }
    static var button2Color: UIColor {
        #colorLiteral(red: 0.3989646435, green: 0.07909781486, blue: 0.9998537898, alpha: 1)
    }
    static var selectedTabBarItemColor: UIColor {
        UIColor(named: "purple1")!
    }
    static var lightGray: UIColor {
        #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
    }
}
