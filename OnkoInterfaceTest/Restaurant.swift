//
//  Restaurant.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 16.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit

struct Restaurant {
    let title: String
    let rating: String
    let image: UIImage
    let discript: String
    let location: Location
}
