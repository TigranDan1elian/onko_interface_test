//
//  SceneDelegate.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 02.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(windowScene: windowScene)
        
        
        let ls = LocationService()
        
        let viewModel = ViewModel(
            restaurant: Restaurant(title: "Red Fox", rating: "0.1", image: UIImage(named: "redFoxImage")!, discript: "ergfrefg", location: Location(x: 45, y: 40)),
            service: ls
            )
        let mainVC = MainViewController()
        let eventsVC = ExampleViewController()
        eventsVC.configureWith(bindings: viewModel).dispose()
        let studyVC = UIViewController()
        let bookmarksVC = UIViewController()
        let settingsVC = UIViewController()
        let tabBarController = UITabBarController()
        
        mainVC.tabBarItem = UITabBarItem(title: "Главная", image: UIImage(named: "home"), tag: 0)
        eventsVC.tabBarItem = UITabBarItem(title: "События", image: UIImage(named: "calendar") , tag: 1)
        studyVC.tabBarItem = UITabBarItem(title: "Обучение", image: UIImage(named: "study") , tag: 2)
        bookmarksVC.tabBarItem = UITabBarItem(title: "Закладки", image: UIImage(named: "bookmark") , tag: 3)
        settingsVC.tabBarItem = UITabBarItem(title: "Настройки", image: UIImage(named: "user") , tag: 4)
        
        tabBarController.tabBar.barTintColor = .white
        tabBarController.tabBar.tintColor = .selectedTabBarItemColor
        
        let controllers = [mainVC, eventsVC, studyVC, bookmarksVC, settingsVC]
        tabBarController.setViewControllers(controllers.map{UINavigationController(rootViewController: $0)}, animated: true)
        
        window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
    }
}

