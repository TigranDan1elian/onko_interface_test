//
//  ViewController.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 02.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

private let headerIdentifier = "HeaderView"
private let cellIdentifier = "Cell"

class MainViewController: UIViewController {
    
    // MARK:- Properties
    
    struct Shortcut  {
        let image: UIImage!
        let title: String
    }
    
    let items = [Shortcut(image: UIImage(named: "news"), title: "Новости"),
                 Shortcut(image: UIImage(named: "press"), title: "Пресса"),
                 Shortcut(image: UIImage(named: "recommended"), title: "Клинические рекомендации и стандарты"),
                 Shortcut(image: UIImage(named: "ZNO"), title: "Стадирование ЗНО"),
                 Shortcut(image: UIImage(named: "MGD"), title: "Молекулярно-генетическая диагностика"),
                 Shortcut(image: UIImage(named: "classification"), title: "Морфологическая классификация"),
                 Shortcut(image: UIImage(named: "calculator"), title: "Калькуляторы и шкалы"),
                 Shortcut(image: UIImage(named: "unwanted"), title: "Нежелательные явления"),
                 Shortcut(image: UIImage(named: "asessment")    , title: "Оценка ответа на лечение")
    ]
    
    let collectionView = UICollectionView(frame: .zero , collectionViewLayout: UICollectionViewFlowLayout())
    
    private let disposeBag = DisposeBag()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        observableExamples()
        setupViews()
        onItemTapHandler(color: .lightGray)
    }
    
    //MARK:- Views & Constraints
    
    func add(x: Int) -> (Int) -> Int {
        return { i in i + 10 }
    }
    
    func observableExamples() {
        
        // Написать Observable из 1 элемента
        
        let one = Observable<Int>.just(5)
        
        // Написать o: Observable из 5 элементов
        
        let o = Observable.of(3, 6, 7, 10, 54)
        
        // Взять функцию add(x: Int) -> (Int) -> (Int) и добавить ко всем элементам o 10
        
        o.map(add(x: 10))
            .subscribe(onNext: { print($0) },
                       onCompleted: { print("complete")} )
            .disposed(by: disposeBag)
        
        // Трансформировать каждый элемент o в поток чисел от 0 до e, где e - это очередной элемент из o
        
        o.flatMap { val in
            Observable.of(0...val)
        }
        .subscribe(onNext: { print($0) },
                   onCompleted: { print("complete")} )
            .disposed(by: disposeBag)
        
        // Выводить в результирующий поток только числа, которые делятся на 7
        
        o.filter { $0 % 7 == 0 }
            .subscribe(onNext: { print($0) },
                       onCompleted: { print("complete")} )
            .disposed(by: disposeBag)
        
        // Вывести только первое и последнее число из этого потока
        
        o.toArray().asObservable().map({ ($0.first!, $0.last!) })
            .subscribe(onNext: { print($0) },
                       onCompleted: { print("complete")} )
            .disposed(by: disposeBag)
        
        // Взять два потока строк и вывести объединения пар
        
        let names = Observable.of("John", "Elly", "Alex", "Peter")
        let jobs = Observable.of("Manager", "Developer", "Designer", "Artist")
               
        
        let combined = Observable.zip(names, jobs)
        combined.subscribe(onNext: { print($0) },
                           onCompleted: { print("complete")} )
            .disposed(by: disposeBag)
        
        // Один поток - число 3, второй поток - числа от 0 до 10. Вывести поток вторых чисел, умноженных на первый поток
        
        let three = Observable.just(3)
        let numbers = Observable.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        
        let results = Observable.combineLatest(three, numbers, resultSelector: {$0 * $1})
        results.subscribe(onNext: { print($0) },
                          onCompleted: { print("complete")} )
            .disposed(by: disposeBag)
        
        // Добавить в первый поток еще одно число и посмотреть на вывод, объяснить что произошло
        
        let threeAndFour = Observable.of(3, 4)
        
        let results2 = Observable.combineLatest(threeAndFour, numbers, resultSelector: {$0 * $1})
        results2.subscribe (onNext: { print($0) },
                            onCompleted: { print("complete")} )
            .disposed(by: disposeBag)
        
        /* вывод -
         0
         0
         4
         8
         12
         16
         20
         24
         28
         32
         36
         40
         
         Новый поток генерируется из элементов потока threeAndFour, помноженных на каждый элемент потока numbers, в порядке появления:
         3 * 0
         4 * 0
         4 * 1
         4 * 2
         4 * 3
         ...и так далее
         */
        
        
        // Написать биндер для SomeView, который принимает UIColor и задает backgroundColor у myView (задача адаптирована под данный проект)
    
        var vcBgColor: Binder<UIColor> {
            return Binder(self, binding: { vc, event in
                vc.view.backgroundColor = event
            })
        }
        
        vcBgColor.onNext(.green)
        
        // Написать биндер для SomeView, который принимает строку и задает styledText у titleLabel (задача адаптирована под данный проект)
        // Написать биндер для SomeView, который принимает ViewStyle и применяет его к SomeView
        // Написать Driver, который отдает UIColor, трансформирует его в _tintColor и применяет к SomeView
        
    }
    
    func setupViews() {
        
        //view.backgroundColor = .white
        self.title = "Главная"
        
        collectionView.register(HeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerIdentifier)
        collectionView.register(CollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInsetAdjustmentBehavior = .never
        
        view.add(collectionView
            .backgroundColor(.lightGray)) {
                $0.leading.trailing.equalToSuperview()
                $0.bottom.equalTo(self.view.safeAreaLayoutGuide)
                $0.top.equalTo(self.view.safeAreaLayoutGuide)
        }
    }
}

// MARK: - Collection View Data Source, Delegate & FlowLayout

extension MainViewController:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerIdentifier, for: indexPath)
        return header
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        CGSize(width: collectionView.frame.width, height: view.frame.height * 0.30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        cell.textLabel.text = items[indexPath.row].title.uppercased()
        cell.imageView.image = items[indexPath.row].image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSize = CGSize(width: (view.bounds.width - 35) / 2, height: view.bounds.width / 2.7)
        return itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let insets = UIEdgeInsets(top: 0, left: 12, bottom: 23, right: 12)
        return insets
    }
    
    func onItemTapHandler(color: UIColor) {
        let newVC = UIViewController()
        newVC.view.backgroundColor = color
        collectionView.rx.itemSelected.subscribe { _ in
            self.present(newVC, animated: true)
            print("tap")
        }.disposed(by: disposeBag)
    }
    
    
    
}
