//
//  LocationService.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 16.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class LocationService: LocationServiceProtocol {
    func current() -> Observable<Location> {
        let loc = Location(x: 43, y: 50)
        return Observable.just(loc)
    }
}
