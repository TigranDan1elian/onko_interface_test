//
//  ViewBindingsImpl.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 16.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

struct Location {
    let x: Int
    let y: Int
}

protocol LocationServiceProtocol {
    func current() -> Observable<Location>
}


class ViewModel: ViewBindings {
    
    var title: Driver<String>
    var location: Driver<String>
    var rating: Driver<String>
    var image: Driver<UIImage>
    var discript: Driver<String>
    
    init(restaurant: Restaurant, service: LocationServiceProtocol) {
        title = Driver.just(restaurant.title)
        rating = Driver.just(restaurant.rating)
        image = Driver.just(restaurant.image)
        discript = Driver.just(restaurant.discript)
        location = service.current()
        .map({ myLocation -> String in
            print("myLocation:  \(myLocation)")
            print("restaurant.location: \(restaurant.location)")
            let restaurantLocation = restaurant.location
            let locX = myLocation.x - restaurantLocation.x
            let locY = myLocation.y - restaurantLocation.y
            return String(locX * locY)
            }).asDriver(onErrorJustReturn: "")

    }
    
}
