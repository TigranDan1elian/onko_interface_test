//
//  ExampleVewController.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 14.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SnapKit

protocol ViewBindings {
    var title: Driver<String> { get }
    var location: Driver<String> { get }
    var rating: Driver<String> { get }
    var image: Driver<UIImage> { get }
    var discript: Driver<String> { get }
}

class ExampleViewController: UIViewController {
    
    var bindings: ViewBindings!
    
    let disposeBag = DisposeBag()
    
    let headerView = UIView()
    let mainImage = UIImageView()
    let titleLabel = UILabel()
    let discriptionLabel = UILabel()
    let ratingLabel = UILabel()
    let locationLabel = UILabel()
    let starIcon = UIImageView()
    let locationIcon = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
    }
    
    func configureWith(bindings: ViewBindings) -> Disposable {
        self.bindings = bindings
        
        return CompositeDisposable(disposables: [
            bindings.title >>> titleLabel.rx.text,
            bindings.image >>> mainImage.rx.image,
            bindings.location >>> locationLabel.rx.text,
            bindings.rating >>> ratingLabel.rx.text,
            bindings.discript >>> discriptionLabel.rx.text
        ])
    }
    
    
    func setupViews() {
        
        view.add(headerView.backgroundColor(.white)) {
            $0.width.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.height.equalTo(121)
        }
        
        let ratingStack = HStack([starIcon.image(UIImage(named: "Star")),
                                  ratingLabel.fontSize(14)
        ])
        
        let locationStack = HStack([locationIcon.image(UIImage(named: "Union")),
                                    locationLabel.fontSize(14)
        ])
        
        let ratinAndLocationStack = HStack([ratingStack.distribution(.fill).spacing(7).alignment(.center),
                                            locationStack.distribution(.fill).spacing(7).alignment(.center)
        ])
        
        let textStack = VStack([titleLabel.boldFont(16),
                                ratinAndLocationStack.distribution(.fill).alignment(.bottom).alpha(0.2).spacing(15),
                                discriptionLabel.fontSize(14).alpha(0.4).numberOfLines(0)
        ])
        
        headerView.add(HStack([mainImage.cornerRadius(8).width(64),
                               textStack.spacing(4).distribution(.fillProportionally).alignment(.leading)
        ])
            .spacing(18)
            .distribution(.fill)) {
                $0.edges.equalToSuperview().inset(20)
        }
        
    }
    
}
