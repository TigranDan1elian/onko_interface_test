//
//  Layout.swift
//  smartpot
//
//  Created by Danil Lahtin on 16/06/2019.
//

import Foundation
import UIKit
import SnapKit



public protocol Transformable {
    typealias Transform = (inout Self) -> ()
}

extension Transformable {
    public func transform(_ transform: Transform) -> Self {
        var mutable = self
        transform(&mutable)
        
        return mutable
    }
}


extension Float: Transformable {}
extension Double: Transformable {}
extension Int: Transformable {}
extension String: Transformable {}
extension Bool: Transformable {}
extension CGSize: Transformable {}
extension CGPoint: Transformable {}
extension CGRect: Transformable {}
extension UIEdgeInsets: Transformable {}
extension NSObject: Transformable {}
extension IndexPath: Transformable {}
extension Optional: Transformable {}
extension Array: Transformable {}
extension Set: Transformable {}
extension Dictionary: Transformable {}


func HStack(_ subviews: [UIView] = []) -> UIStackView {
    let stack = UIStackView(arrangedSubviews: subviews)
    stack.axis = .horizontal
    
    return stack
}


func VStack(_ subviews: [UIView] = []) -> UIStackView {
    let stack = UIStackView(arrangedSubviews: subviews)
    stack.axis = .vertical
    
    return stack
}


extension NSLayoutConstraint {
    @discardableResult
    func priority(_ priority: UILayoutPriority) -> NSLayoutConstraint {
        self.priority = priority
        
        return self
    }
}


protocol UIViewProtocol: class, Transformable {
    var backgroundColor: UIColor? { get set }
    var tintColor: UIColor! { get set }
    
    var leadingAnchor: NSLayoutXAxisAnchor { get }
    var trailingAnchor: NSLayoutXAxisAnchor { get }
    var leftAnchor: NSLayoutXAxisAnchor { get }
    var rightAnchor: NSLayoutXAxisAnchor { get }
    var topAnchor: NSLayoutYAxisAnchor { get }
    var bottomAnchor: NSLayoutYAxisAnchor { get }
    var widthAnchor: NSLayoutDimension { get }
    var heightAnchor: NSLayoutDimension { get }
    var centerXAnchor: NSLayoutXAxisAnchor { get }
    var centerYAnchor: NSLayoutYAxisAnchor { get }
    var firstBaselineAnchor: NSLayoutYAxisAnchor { get }
    var lastBaselineAnchor: NSLayoutYAxisAnchor { get }
    
    var isUserInteractionEnabled: Bool { get set }
    var transform: CGAffineTransform { get set }
    var clipsToBounds: Bool { get set }
    var alpha: CGFloat { get set }
    var isOpaque: Bool { get set }
    var isHidden: Bool { get set }
    var contentMode: UIView.ContentMode { get set }
    var mask: UIView? { get set }
    var tintAdjustmentMode: UIView.TintAdjustmentMode { get set }
    var translatesAutoresizingMaskIntoConstraints: Bool { get set }
    var layer: CALayer { get }
    var layoutMargins: Insets { get set }
    
    func addSubview(_ view: UIView)
    func setContentHuggingPriority(_ priority: UILayoutPriority, for axis: NSLayoutConstraint.Axis)
    func setContentCompressionResistancePriority(_ priority: UILayoutPriority, for axis: NSLayoutConstraint.Axis)
}


extension UIViewProtocol {
    @discardableResult
    func backgroundColor(_ color: UIColor?) -> Self {
        return self.transform({ $0.backgroundColor = color })
    }
    
    @discardableResult
    func tintColor(_ color: UIColor) -> Self {
        return self.transform({ $0.tintColor = color })
    }
    
    @discardableResult
    func isUserInteractionEnabled(_ isUserInteractionEnabled: Bool) -> Self {
        return self.transform({ $0.isUserInteractionEnabled = isUserInteractionEnabled })
    }
    
    @discardableResult
    func transform(_ transform: CGAffineTransform) -> Self {
        return self.transform({ $0.transform = transform })
    }
    
    @discardableResult
    func clipsToBounds(_ clipsToBounds: Bool) -> Self {
        return self.transform({ $0.clipsToBounds = clipsToBounds })
    }
    
    @discardableResult
    func alpha(_ alpha: CGFloat) -> Self {
        return self.transform({ $0.alpha = alpha })
    }
    
    @discardableResult
    func isOpaque(_ isOpaque: Bool) -> Self {
        return self.transform({ $0.isOpaque = isOpaque })
    }
    
    @discardableResult
    func isHidden(_ isHidden: Bool) -> Self {
        return self.transform({ $0.isHidden = isHidden })
    }
    
    @discardableResult
    func contentMode(_ contentMode: UIView.ContentMode) -> Self {
        return self.transform({ $0.contentMode = contentMode })
    }
    
    @discardableResult
    func mask(_ mask: UIView?) -> Self {
        return self.transform({ $0.mask = mask })
    }
    
    
    @discardableResult
    func layoutMargins (_ layoutMargins: Insets) -> Self {
        return self.transform({ $0.layoutMargins = layoutMargins })
    }
    
    
    
    @discardableResult
    func tintAdjustmentMode(_ tintAdjustmentMode: UIView.TintAdjustmentMode) -> Self {
        return self.transform({ $0.tintAdjustmentMode = tintAdjustmentMode })
    }
    
    @discardableResult
    func size(_ size: CGSize, priority: UILayoutPriority = .required) -> Self {
        self.widthAnchor.constraint(equalToConstant: size.width).priority(priority).isActive = true
        self.heightAnchor.constraint(equalToConstant: size.height).priority(priority).isActive = true
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        return self
    }
    
    @discardableResult
    func width(_ width: CGFloat, priority: UILayoutPriority = .required) -> Self {
        self.widthAnchor.constraint(equalToConstant: width).priority(priority).isActive = true
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        return self
    }
    
    @discardableResult
    func height(_ height: CGFloat, priority: UILayoutPriority = .required) -> Self {
        self.heightAnchor.constraint(equalToConstant: height).priority(priority).isActive = true
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        return self
    }
    
    @discardableResult
    func hugging(x: UILayoutPriority? = nil, y: UILayoutPriority? = nil) -> Self {
        if let x = x {
            self.setContentHuggingPriority(x, for: .horizontal)
        }
        
        if let y = y {
            self.setContentHuggingPriority(y, for: .vertical)
        }
        
        return self
    }
    
    @discardableResult
    func compression(x: UILayoutPriority? = nil, y: UILayoutPriority? = nil) -> Self {
        if let x = x {
            self.setContentCompressionResistancePriority(x, for: .horizontal)
        }
        
        if let y = y {
            self.setContentCompressionResistancePriority(y, for: .vertical)
        }
        
        return self
    }
    
    @discardableResult
    func add(_ subview: UIView, insets: Insets, priority: UILayoutPriority = .required) -> Self {
        self.addSubview(subview)
        
        subview.leftAnchor
            .constraint(equalTo: self.leftAnchor, constant: insets.left)
            .priority(priority)
            .isActive = true
        subview.rightAnchor
            .constraint(equalTo: self.rightAnchor, constant: -insets.right)
            .priority(priority)
            .isActive = true
        subview.topAnchor
            .constraint(equalTo: self.topAnchor, constant: insets.top)
            .priority(priority)
            .isActive = true
        subview.bottomAnchor
            .constraint(equalTo: self.bottomAnchor, constant: -insets.bottom)
            .priority(priority)
            .isActive = true
        subview.translatesAutoresizingMaskIntoConstraints = false
        
        return self
    }
    
    @discardableResult
    func add(_ subview: UIView, constraintMaker: ((ConstraintMaker) -> Void)? = nil) -> Self {
        self.addSubview(subview)
        
        if let maker = constraintMaker {
            subview.snp.makeConstraints(maker)
        }
        
        return self
    }
    
    // MARK: - Layer
    
    @discardableResult
    func bounds(_ bounds: CGRect) -> Self {
        return self.transform({ $0.layer.bounds = bounds })
    }
    
    @discardableResult
    func position(_ position: CGPoint) -> Self {
        return self.transform({ $0.layer.position = position })
    }
    
    @discardableResult
    func zPosition(_ zPosition: CGFloat) -> Self {
        return self.transform({ $0.layer.zPosition = zPosition })
    }
    
    @discardableResult
    func anchorPoint(_ anchorPoint: CGPoint) -> Self {
        return self.transform({ $0.layer.anchorPoint = anchorPoint })
    }
    
    @discardableResult
    func anchorPointZ(_ anchorPointZ: CGFloat) -> Self {
        return self.transform({ $0.layer.anchorPointZ = anchorPointZ })
    }
    
    @discardableResult
    func layerTransform(_ transform: CATransform3D) -> Self {
        return self.transform({ $0.layer.transform = transform })
    }
    
    @discardableResult
    func isDoubleSided(_ isDoubleSided: Bool) -> Self {
        return self.transform({ $0.layer.isDoubleSided = isDoubleSided })
    }
    
    @discardableResult
    func isGeometryFlipped(_ isGeometryFlipped: Bool) -> Self {
        return self.transform({ $0.layer.isGeometryFlipped = isGeometryFlipped })
    }
    
    @discardableResult
    func sublayers(_ sublayers: [CALayer]?) -> Self {
        return self.transform({ $0.layer.sublayers = sublayers })
    }
    
    @discardableResult
    func sublayerTransform(_ sublayerTransform: CATransform3D) -> Self {
        return self.transform({ $0.layer.sublayerTransform = sublayerTransform })
    }
    
    @discardableResult
    func mask(_ mask: CALayer?) -> Self {
        return self.transform({ $0.layer.mask = mask })
    }
    
    @discardableResult
    func masksToBounds(_ masksToBounds: Bool) -> Self {
        return self.transform({ $0.layer.masksToBounds = masksToBounds })
    }
    
    @discardableResult
    func contents(_ contents: Any?) -> Self {
        return self.transform({ $0.layer.contents = contents })
    }
    
    @discardableResult
    func contentsRect(_ contentsRect: CGRect) -> Self {
        return self.transform({ $0.layer.contentsRect = contentsRect })
    }
    
    @discardableResult
    func contentsGravity(_ contentsGravity: CALayerContentsGravity) -> Self {
        return self.transform({ $0.layer.contentsGravity = contentsGravity })
    }
    
    @discardableResult
    func contentsScale(_ contentsScale: CGFloat) -> Self {
        return self.transform({ $0.layer.contentsScale = contentsScale })
    }
    
    @discardableResult
    func contentsCenter(_ contentsCenter: CGRect) -> Self {
        return self.transform({ $0.layer.contentsCenter = contentsCenter })
    }
    
    @discardableResult
    func contentsFormat(_ contentsFormat: CALayerContentsFormat) -> Self {
        return self.transform({ $0.layer.contentsFormat = contentsFormat })
    }
    
    @discardableResult
    func minificationFilter(_ minificationFilter: CALayerContentsFilter) -> Self {
        return self.transform({ $0.layer.minificationFilter = minificationFilter })
    }
    
    @discardableResult
    func magnificationFilter(_ magnificationFilter: CALayerContentsFilter) -> Self {
        return self.transform({ $0.layer.magnificationFilter = magnificationFilter })
    }
    
    @discardableResult
    func minificationFilterBias(_ minificationFilterBias: Float) -> Self {
        return self.transform({ $0.layer.minificationFilterBias = minificationFilterBias })
    }
    
    @discardableResult
    func needsDisplayOnBoundsChange(_ needsDisplayOnBoundsChange: Bool) -> Self {
        return self.transform({ $0.layer.needsDisplayOnBoundsChange = needsDisplayOnBoundsChange })
    }
    
    @discardableResult
    func drawsAsynchronously(_ drawsAsynchronously: Bool) -> Self {
        return self.transform({ $0.layer.drawsAsynchronously = drawsAsynchronously })
    }
    
    @discardableResult
    func edgeAntialiasingMask(_ edgeAntialiasingMask: CAEdgeAntialiasingMask) -> Self {
        return self.transform({ $0.layer.edgeAntialiasingMask = edgeAntialiasingMask })
    }
    
    @discardableResult
    func allowsEdgeAntialiasing(_ allowsEdgeAntialiasing: Bool) -> Self {
        return self.transform({ $0.layer.allowsEdgeAntialiasing = allowsEdgeAntialiasing })
    }
    
    @discardableResult
    func backgroundColor(_ backgroundColor: CGColor?) -> Self {
        return self.transform({ $0.layer.backgroundColor = backgroundColor })
    }
    
    @discardableResult
    func cornerRadius(_ cornerRadius: CGFloat) -> Self {
        return self.transform({ $0.layer.cornerRadius = cornerRadius })
    }
    
    @available(iOS 11.0, *)
    @discardableResult
    func maskedCorners(_ maskedCorners: CACornerMask) -> Self {
        return self.transform({ $0.layer.maskedCorners = maskedCorners })
    }
    
    @discardableResult
    func borderWidth(_ borderWidth: CGFloat) -> Self {
        return self.transform({ $0.layer.borderWidth = borderWidth })
    }
    
    @discardableResult
    func borderColor(_ borderColor: UIColor?) -> Self {
        return self.transform({ $0.layer.borderColor = borderColor?.cgColor })
    }
    
    @discardableResult
    func opacity(_ opacity: Float) -> Self {
        return self.transform({ $0.layer.opacity = opacity })
    }
    
    @discardableResult
    func allowsGroupOpacity(_ allowsGroupOpacity: Bool) -> Self {
        return self.transform({ $0.layer.allowsGroupOpacity = allowsGroupOpacity })
    }
    
    @discardableResult
    func compositingFilter(_ compositingFilter: Any?) -> Self {
        return self.transform({ $0.layer.compositingFilter = compositingFilter })
    }
    
    @discardableResult
    func filters(_ filters: [Any]?) -> Self {
        return self.transform({ $0.layer.filters = filters })
    }
    
    @discardableResult
    func backgroundFilters(_ backgroundFilters: [Any]?) -> Self {
        return self.transform({ $0.layer.backgroundFilters = backgroundFilters })
    }
    
    @discardableResult
    func shouldRasterize(_ shouldRasterize: Bool) -> Self {
        return self.transform({ $0.layer.shouldRasterize = shouldRasterize })
    }
    
    @discardableResult
    func rasterizationScale(_ rasterizationScale: CGFloat) -> Self {
        return self.transform({ $0.layer.rasterizationScale = rasterizationScale })
    }
    
    @discardableResult
    func shadowColor(_ shadowColor: CGColor?) -> Self {
        return self.transform({ $0.layer.shadowColor = shadowColor })
    }
    
    @discardableResult
    func shadowOpacity(_ shadowOpacity: Float) -> Self {
        return self.transform({ $0.layer.shadowOpacity = shadowOpacity })
    }
    
    @discardableResult
    func shadowOffset(_ shadowOffset: CGSize) -> Self {
        return self.transform({ $0.layer.shadowOffset = shadowOffset })
    }
    
    @discardableResult
    func shadowRadius(_ shadowRadius: CGFloat) -> Self {
        return self.transform({ $0.layer.shadowRadius = shadowRadius })
    }
    
    @discardableResult
    func shadowPath(_ shadowPath: CGPath?) -> Self {
        return self.transform({ $0.layer.shadowPath = shadowPath })
    }
    
    @discardableResult
    func layerName(_ name: String?) -> Self {
        return self.transform({ $0.layer.name = name })
    }
    
    @discardableResult
    func layerDelegate(_ delegate: CALayerDelegate?) -> Self {
        return self.transform({ $0.layer.delegate = delegate })
    }
    
    @discardableResult
    func style(_ style: [AnyHashable: Any]?) -> Self {
        return self.transform({ $0.layer.style = style })
    }
}


extension UIView: UIViewProtocol {}


extension UIStackView {
    @discardableResult
    func spacing(_ value: CGFloat) -> UIStackView {
        self.spacing = value
        return self
    }
    
    @discardableResult
    func alignment(_ alignment: UIStackView.Alignment) -> UIStackView {
        self.alignment = alignment
        return self
    }
    
    @discardableResult
    func distribution(_ distribution: UIStackView.Distribution) -> UIStackView {
        self.distribution = distribution
        return self
    }
}


extension UILabel {
    
    @discardableResult
    func text(_ text: String) -> UILabel {
        self.text = text
        
        return self
    }
    
    @discardableResult
    func numberOfLines(_ count: Int) -> UILabel {
        self.numberOfLines = count
        
        return self
    }
    
    @discardableResult
    func fontSize(_ size: CGFloat) -> UILabel {
        self.font = UIFont(name: "Arial", size: size)
        
        return self
    }
    
    @discardableResult
    func fontColor(_ color: UIColor) -> UILabel {
        self.textColor = color
        
        return self
    }
    
    @discardableResult
    func boldFont(_ size: CGFloat) -> UILabel {
        self.font = UIFont.boldSystemFont(ofSize: size)
        
        return self
    }
    
    
    
    @discardableResult
    func textAlignment(_ textAlignment: NSTextAlignment) -> UILabel {
        self.textAlignment = textAlignment
        
        return self
    }
}


extension UIButton {
    
    
    @discardableResult
    func imageEdgeInsets(_ imageEdgeInsets: Insets) -> Self {
        self.imageEdgeInsets = imageEdgeInsets
        
        return self
    }
    
    @discardableResult
    func title(_ text: String) -> Self {
        setTitle(text, for: .normal)
        return self
    }
    
    @discardableResult
    func contentEdgeInsets(_ contentEdgeInsets: Insets) -> Self {
        self.contentEdgeInsets = contentEdgeInsets
        
        return self
    }
    
    @discardableResult
    @objc
    func image(_ image: UIImage?, for state: UIControl.State = .normal) -> Self {
        setImage(image, for: state)
        
        return self
    }
}


extension UILayoutPriority: ExpressibleByFloatLiteral {
    public typealias FloatLiteralType = Float
    
    public init(floatLiteral value: Float) {
        self.init(rawValue: value)
    }
}


extension UILayoutPriority: ExpressibleByIntegerLiteral {
    public typealias IntegerLiteralType = Int
    
    public init(integerLiteral value: Int) {
        self.init(rawValue: Float(value))
    }
}


extension UILayoutPriority {
    static let high: UILayoutPriority = 900
}


extension UIImageView {
    @discardableResult
    func image(_ image: UIImage?) -> UIImageView {
        self.image = image
        
        return self
    }
}

extension UIControl {
    @discardableResult
    func target(_ target: Any?, action: Selector, for controlEvents: UIControl.Event = .touchUpInside) -> Self {
        self.addTarget(target, action: action, for: controlEvents)
        
        return self
    }
}

extension UITableView {
    @discardableResult
    func tableHeaderView(_ tableHeaderView: UIView?) -> Self {
        self.tableHeaderView = tableHeaderView
        
        return self
    }
    
    @discardableResult
    func separatorStyle(_ separatorStyle: UITableViewCell.SeparatorStyle) -> Self {
        self.separatorStyle = separatorStyle
        
        return self
    }
    
    @discardableResult
    func separatorColor(_ separatorColor: UIColor) -> Self {
        self.separatorColor = separatorColor
        
        return self
    }
    
    @discardableResult
    func separatorInset(_ separatorInset: Insets) -> Self {
        self.separatorInset = separatorInset
        
        return self
    }
    
    @discardableResult
    func rowHeight(_ rowHeight: CGFloat) -> Self {
        self.rowHeight = rowHeight
        
        return self
    }
    
    @discardableResult
    func scrollIndicatorInsets(_ scrollIndicatorInsets: Insets) -> Self {
        self.scrollIndicatorInsets = scrollIndicatorInsets
        
        return self
    }
    
    @discardableResult
    func sectionIndexColor(_ sectionIndexColor: UIColor?) -> Self {
        self.sectionIndexColor = sectionIndexColor
        
        return self
    }
    
}


extension UISearchBar {
    @discardableResult
    func placeholder(_ placeholder: String?) -> Self {
        self.placeholder = placeholder
        
        return self
    }
    
    @discardableResult
    func barTintColor(_ barTintColor: UIColor?) -> Self {
        self.barTintColor = barTintColor
        
        return self
    }
}


extension UITextField {
    @discardableResult
    func borderStyle(_ borderStyle: UITextField.BorderStyle) -> Self {
        self.borderStyle = borderStyle
        
        return self
    }
    
    @discardableResult
    func leftViewMode(_ leftViewMode: UITextField.ViewMode) -> Self {
        self.leftViewMode = leftViewMode
        
        return self
    }
    
    @discardableResult
    func rightViewMode(_ rightViewMode: UITextField.ViewMode) -> Self {
        self.rightViewMode = rightViewMode
        
        return self
    }
    
    @discardableResult
    func clearButtonMode(_ clearButtonMode: UITextField.ViewMode) -> Self {
        self.clearButtonMode = clearButtonMode
        
        return self
    }
    
    @discardableResult
    func leftView(_ leftView: UIView?) -> Self {
        self.leftView = leftView
        
        return self
    }
    
    @discardableResult
    func rightView(_ rightView: UIView?) -> Self {
        self.rightView = rightView
        
        return self
    }
}

extension UIScrollView {
    @discardableResult
    func contentInset(_ contentInset: Insets) -> Self {
        self.contentInset = contentInset
        
        return self
    }
}

extension UIPageControl {
    @discardableResult
    func pageIndicatorTintColor(_ pageIndicatorTintColor: UIColor?) -> Self {
        self.pageIndicatorTintColor = pageIndicatorTintColor
        return self
    }
    
    @discardableResult
    func currentPageIndicatorTintColor(_ currentPageIndicatorTintColor: UIColor?) -> Self {
        self.currentPageIndicatorTintColor = currentPageIndicatorTintColor
        return self
    }
    
    @discardableResult
    func hidesForSinglePage(_ hidesForSinglePage: Bool) -> Self {
        self.hidesForSinglePage = hidesForSinglePage
        
        return self
    }
}

extension UICollectionView {
    @discardableResult
    func delegate(_ delegate: UICollectionViewDelegate) -> Self {
        self.delegate = delegate
        
        return self
    }
    
    @discardableResult
    func dataSource(_ dataSource: UICollectionViewDataSource) -> Self {
        self.dataSource = dataSource
        
        return self
    }
    
    @discardableResult
    func isPagingEnabled(_ isPagingEnabled: Bool) -> Self {
        self.isPagingEnabled = isPagingEnabled
        
        return self
    }
    
    @discardableResult
    func showsHorizontalScrollIndicator(_ showsHorizontalScrollIndicator: Bool) -> Self {
        self.showsHorizontalScrollIndicator = showsHorizontalScrollIndicator
        
        return self
    }
    
    @discardableResult
    func showsVerticalScrollIndicator(_ showsVerticalScrollIndicator: Bool) -> Self {
        self.showsVerticalScrollIndicator = showsVerticalScrollIndicator
        
        return self
    }
    
    @discardableResult
    func bounces(_ bounces: Bool) -> Self {
        self.bounces = bounces
        
        return self
    }
}

typealias Insets = UIEdgeInsets


extension UIEdgeInsets {
    init(inset: CGFloat) {
        self.init(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    static func top(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: value, left: 0, bottom: 0, right: 0)
    }
    
    static func left(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: value, bottom: 0, right: 0)
    }
    
    static func bottom(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: value, right: 0)
    }
    
    static func right(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: value)
    }
    
    static func side(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: value, bottom: 0, right: value)
    }
    
    static func horizontal(_ value: CGFloat) -> UIEdgeInsets {
        return side(value)
    }
    
    static func vertical(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: value, left: 0, bottom: value, right: 0)
    }
    
    static func + (lhs: UIEdgeInsets, rhs: UIEdgeInsets) -> UIEdgeInsets {
        return lhs.transform({
            $0.top += rhs.top
            $0.bottom += rhs.bottom
            $0.left += rhs.left
            $0.right += rhs.right
        })
    }
}


extension UIEdgeInsets: ExpressibleByIntegerLiteral {
    public init(integerLiteral value: Int) {
        self.init(inset: CGFloat(value))
    }
}


extension UIEdgeInsets: ExpressibleByFloatLiteral {
    public init(floatLiteral value: Float) {
        self.init(inset: CGFloat(value))
    }
}
