//
//  Operators.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 14.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

infix operator >>>: AssignmentPrecedence

extension ObservableConvertibleType {
    public static func >>><Observer: ObserverType> (o: Self, observer: Observer)
        -> Disposable where Observer.Element == Element {
        return o.asObservable().subscribe(observer)
    }
    
    public static func >>><Observer: ObserverType> (o: Self, observer: Observer)
        -> Disposable where Observer.Element == Element? {
            return o.asObservable().map({ $0 as Element? }).subscribe(observer)
    }
    
    public static func >>> (o: Self, relay: BehaviorRelay<Element>) -> Disposable {
        return o.asObservable().bind(to: relay)
    }
    
    public static func >>> (o: Self, relay: BehaviorRelay<Element?>) -> Disposable {
        return o.asObservable().bind(to: relay)
    }
    
    public static func >>> (o: Self, relay: PublishRelay<Element>) -> Disposable {
        return o.asObservable().bind(to: relay)
    }
    
    public static func >>> (o: Self, relay: PublishRelay<Element?>) -> Disposable {
        return o.asObservable().bind(to: relay)
    }
    
    public static func >>><R> (o: Self, _ transformation: @escaping (Observable<Element>) -> R) -> R {
        return transformation(o.asObservable())
    }
    
    public static func += (disposeBag: DisposeBag, o: Self) {
        return o.asObservable().subscribe().disposed(by: disposeBag)
    }
}

extension ObservableConvertibleType {
    public static func >>> (o: Self, onNext: @escaping (Element) -> ()) -> Disposable {
        return o.asObservable().subscribe(onNext: onNext)
    }
}

extension DisposeBag {
    public static func += (disposeBag: DisposeBag, disposable: Disposable) {
        disposeBag.insert(disposable)
    }
}
