//
//  CollectionViewCell.swift
//  OnkoInterfaceTest
//
//  Created by Tigran Danielian on 02.07.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit
import SnapKit

class CollectionViewCell: UICollectionViewCell {
    
    var textLabel = UILabel()
    var imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        
        backgroundColor = .white
        layer.cornerRadius = 10
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 4.0, height: 4.0)
        layer.shadowRadius = 5.0
        layer.shadowOpacity = 0.1
        
        add(textLabel
            .numberOfLines(0)
            .boldFont(contentView.bounds.height / 11.5)
            .fontColor(.black)) {
            $0.bottom.trailing.leading.equalToSuperview().inset(12)
        }
        add(imageView) {
            $0.top.trailing.equalToSuperview().inset(12)
            $0.height.width.equalTo(56)
        }
    }
}
